package cn.dreamtobe.percentsmoothhandler.demo;

import cn.dreamtobe.percentsmoothhandler.SmoothHandler;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;

public class MainAbility extends Ability implements Component.ClickedListener {
    private SmoothProgressBar smoothPb;
    private Slider increaseDurationSmoothSb;
    private Button increaseDurationSmoothBtn;
    private Button increaseSmoothlyBtn;
    private Button increaseNormalBtn;
    private Button resetBtn;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        assignViews();
        smoothPb.setSmoothPercent(0.3f);

        increaseDurationSmoothSb.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int index, boolean flag) {
            }

            @Override
            public void onTouchStart(Slider slider) {
            }

            @Override
            public void onTouchEnd(Slider slider) {
                increaseDurationSmoothBtn.setText(
                        getString(ResourceTable.String_increase_duration_definite_smoothly, getDuration()));
            }
        });

        increaseDurationSmoothBtn.setText(
                getString(ResourceTable.String_increase_duration_definite_smoothly, getDuration()));
    }

    public void onClickNormalIncrease(final Component view) {
        smoothPb.setPercent(getIncreasePercent());
    }

    public void onClickSmoothIncrease(final Component view) {
        smoothPb.setSmoothPercent(getIncreasePercent());
    }

    public void onClickSmoothDurationIncrease(final Component view) {
        smoothPb.setSmoothPercent(getIncreasePercent(), getDuration());
    }

    public void onClickRest(final Component view) {
        smoothPb.setPercent(0);
    }

    private int getDuration() {
        return increaseDurationSmoothSb.getProgress() * 100;
    }

    private float getIncreasePercent() {
        float targetPercent = smoothPb.getPercent() + 0.1f;
        return Math.min(1, targetPercent);
    }

    private void assignViews() {
        smoothPb = (SmoothProgressBar) findComponentById(ResourceTable.Id_smooth_pb);
        increaseDurationSmoothSb = (Slider) findComponentById(ResourceTable.Id_increase_duration_smooth_sb);
        increaseDurationSmoothBtn = (Button) findComponentById(ResourceTable.Id_increase_duration_smooth_btn);
        increaseSmoothlyBtn = (Button) findComponentById(ResourceTable.Id_increase_smoothly_btn);
        increaseNormalBtn = (Button) findComponentById(ResourceTable.Id_increase_normal_btn);
        resetBtn = (Button) findComponentById(ResourceTable.Id_reset_btn);
        increaseDurationSmoothBtn.setClickedListener(this);
        increaseSmoothlyBtn.setClickedListener(this);
        increaseNormalBtn.setClickedListener(this);
        resetBtn.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_increase_duration_smooth_btn:
                onClickSmoothDurationIncrease(component);
                break;
            case ResourceTable.Id_increase_smoothly_btn:
                onClickSmoothIncrease(component);
                break;
            case ResourceTable.Id_increase_normal_btn:
                onClickNormalIncrease(component);
                break;
            case ResourceTable.Id_reset_btn:
                onClickRest(component);
                break;
            default:
                break;
        }
    }
}
