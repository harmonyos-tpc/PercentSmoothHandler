/*
 * Copyright (c) 2016 Jacksgong(blog.dreamtobe.cn).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.dreamtobe.percentsmoothhandler;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.miscservices.timeutility.Time;

import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Jacksgong on 2/2/16.
 * <p/>
 * handle the case of the internal of the percent between the current and the last is
 * too large to smooth for the target
 *
 * @see ISmoothTarget
 */
public class SmoothHandler extends EventHandler {
    final WeakReference<ISmoothTarget> targetWeakReference;
    private static final Logger logger = Logger.getLogger(SmoothHandler.class.getName());
    private float aimPercent;
    private float minInternalPercent = 0.03f; // 3%
    private float smoothInternalPercent = 0.01f; // 1%
    private int smoothIncreaseDelayMillis = 1; // 1ms
    public static boolean NEED_LOG = false;
    private long tempStartTimestamp;
    private long tempDurationMillis;
    private long tempRemainDurationMillis;
    private long tempLastConsumeMillis;
    private boolean tempWarnedAccuracyProblem;
    private final float ALLOWED_PRECISION_ERROR = 0.00001f;

    public float getMinInternalPercent() {
        return minInternalPercent;
    }

    /**
     * if the provider percent more than minInternalPercent, it will be split to the several smoothInternalPercent
     *
     * @param minInternalPercent the min internal of the percent, default 0.03
     * @see #setSmoothInternalPercent(float)
     */
    public void setMinInternalPercent(float minInternalPercent) {
        this.minInternalPercent = minInternalPercent;
    }

    public float getSmoothInternalPercent() {
        return smoothInternalPercent;
    }

    /**
     * if the provider percent more than minInternalPercent, it will be split to the several smoothInternalPercent
     *
     * @param smoothInternalPercent the internal of the percent will provide the smooth effect, default 0.01
     * @see #setMinInternalPercent(float)
     */
    public void setSmoothInternalPercent(float smoothInternalPercent) {
        this.smoothInternalPercent = smoothInternalPercent;
    }

    public int getSmoothIncreaseDelayMillis() {
        return smoothIncreaseDelayMillis;
    }

    public void setSmoothIncreaseDelayMillis(int smoothIncreaseDelayMillis) {
        this.smoothIncreaseDelayMillis = smoothIncreaseDelayMillis;
    }

    /**
     * generally use for the progress widget
     *
     * @param targetWeakReference the weak reference of the smooth target
     */
    public SmoothHandler(WeakReference<ISmoothTarget> targetWeakReference) {
        this(targetWeakReference, EventRunner.getMainEventRunner());
    }

    public SmoothHandler(WeakReference<ISmoothTarget> targetWeakReference, EventRunner eventRunner) {
        super(eventRunner);
        this.targetWeakReference = targetWeakReference;
        this.aimPercent = targetWeakReference.get().getPercent();
        clear();
    }

    @Override
    protected void processEvent(InnerEvent event) {
        super.processEvent(event);
        if (this.targetWeakReference == null || this.targetWeakReference.get() == null) {
            return;
        }
        final ISmoothTarget target = targetWeakReference.get();
        final float currentPercent = target.getPercent();
        final float desiredPercentDelta = calculatePercent(currentPercent);
        setPercent2Target(Math.min(currentPercent + desiredPercentDelta, aimPercent));
        final float realPercentDelta = target.getPercent() - currentPercent;
        if (target.getPercent() >= this.aimPercent || target.getPercent() >= 1 ||
                (target.getPercent() == 0 && this.aimPercent == 0)) {
            if (NEED_LOG) {
                logger.log(Level.SEVERE, String.format(Locale.CHINESE, "finish aimPercent(%f) durationMillis(%d)",
                        this.aimPercent, this.tempDurationMillis));
            }
            clear();
            return;
        }
        long ll = calculateDelay(realPercentDelta, desiredPercentDelta);
        sendEvent(0, ll);
    }

    private void clear() {
        resetTempDelay();
        this.ignoreCommit = false;
        removeEvent(0);
    }

    private boolean ignoreCommit = false;

    /**
     * Must be invoked by some method which will change the percent for monitor all changes
     * about percent.
     *
     * @param percent the percent will be effect by the target.
     */
    public void commitPercent(float percent) {
        if (this.ignoreCommit) {
            this.ignoreCommit = false;
            return;
        }
        this.aimPercent = percent;
    }

    private void setPercent2Target(final float percent) {
        if (targetWeakReference == null || targetWeakReference.get() == null) {
            return;
        }
        this.ignoreCommit = true;
        targetWeakReference.get().setPercent(percent);
        this.ignoreCommit = false;
    }

    public void loopSmooth(float percent) {
        loopSmooth(percent, -1);
    }

    /**
     * If the provider percent(the aim percent) more than {@link #minInternalPercent}, it will
     * be split to the several {@link #smoothInternalPercent}.
     *
     * @param percent        The aim percent.
     * @param durationMillis Temporary duration for {@code percent}. If lesson than 0, it will be
     *                       ignored.
     */
    public void loopSmooth(float percent, long durationMillis) {
        if (this.targetWeakReference == null || this.targetWeakReference.get() == null) {
            return;
        }
        if (NEED_LOG) {
            logger.log(Level.SEVERE,
                    String.format("start loopSmooth lastAimPercent(%f), aimPercent(%f)" +
                            " durationMillis(%d)", aimPercent, percent, durationMillis));
        }
        final ISmoothTarget target = targetWeakReference.get();
        setPercent2Target(this.aimPercent);
        clear();
        this.aimPercent = percent;
        if (this.aimPercent - target.getPercent() > minInternalPercent) {
            if (durationMillis >= 0) {
                tempStartTimestamp = Time.getRealActiveTime();
                tempDurationMillis = durationMillis;
                tempRemainDurationMillis = durationMillis;
            }
            sendEvent(0);
        } else {
            setPercent2Target(percent);
        }
    }

    private void resetTempDelay() {
        tempLastConsumeMillis = smoothIncreaseDelayMillis;
        tempStartTimestamp = -1;
        tempDurationMillis = -1;
        tempRemainDurationMillis = -1;
        tempWarnedAccuracyProblem = false;
    }

    private float calculatePercent(final float currentPercent) {
        if (tempDurationMillis < 0) {
            return smoothInternalPercent;
        }
        float internalPercent;
        final long usedDuration = Time.getRealActiveTime() - tempStartTimestamp;
        final long lastRemainDurationMillis = tempRemainDurationMillis;
        tempRemainDurationMillis = tempDurationMillis - usedDuration;
        tempLastConsumeMillis = Math.max(lastRemainDurationMillis - tempRemainDurationMillis, 1);
        final long splitByDelay = Math.max(tempRemainDurationMillis / tempLastConsumeMillis, 1);
        final float percentDelta = this.aimPercent - currentPercent;
        internalPercent = percentDelta / splitByDelay;
        return internalPercent;
    }

    private long calculateDelay(final float realPercentDelta, final float desiredPercentDelta) {
        if (tempDurationMillis < 0) {
            return smoothIncreaseDelayMillis;
        }
        if (realPercentDelta - desiredPercentDelta <= ALLOWED_PRECISION_ERROR) {
            return smoothIncreaseDelayMillis;
        }
        // Accuracy Problem in target smooth progress.
        if (!tempWarnedAccuracyProblem) {
            tempWarnedAccuracyProblem = true;
            logger.log(Level.SEVERE,
                    String.format("Occur Accuracy Problem in %s, (real percent delta is %f, but" +
                                    " desired percent delta is %f), so we use delay to handle the" +
                                    " temporary duration, as result the processing will not smooth",
                            targetWeakReference.get(), realPercentDelta, desiredPercentDelta));
        }
        return smoothIncreaseDelayMillis;
    }

}