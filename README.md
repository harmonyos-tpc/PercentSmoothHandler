# PercentSmoothHandler

自定义Handler,使得进展更加平滑。

<img src="https://gitee.com/openharmony-tpc/PercentSmoothHandler/raw/master/gifs/smoothHandler.gif" width="50%" height="50%"/>

## 依赖
```
dependencies {
  implementation 'io.openharmony.tpc.thirdlib:PercentSmoothHandler:1.0.0'
}
```

## entry运行要求

通过DevEco studio,并下载SDK

将项目中的build.gradle文件中dependencies→classpath版本改为对应的版本（即你的IDE新建项目中所用的版本）


## 使用

#### Step 1:

1、SmoothProgressBar实现ISmoothTarget接口
2、在SmoothProgressBar中引入，实现setSmoothPercent方法：
    private SmoothHandler smoothHandler;

    @Override
    public void setSmoothPercent(float percent) {
        if (smoothHandler == null) {
            smoothHandler = new SmoothHandler(new WeakReference<ISmoothTarget>(this));
        }
        smoothHandler.loopSmooth(percent);
    }

#### Step 2:

> Invoke the `SmoothHandler#commitPercent` in the method where you will change your percent.

    @Override
    public synchronized void setProgress(int progress) {
        if (smoothHandler != null) {
            smoothHandler.commitPercent(progress / (float) getMax());
        }
        super.setProgress(progress);
    }

#### Step 3:
    @Override
    public void setSmoothPercent(float percent) {
        getSmoothHandler().loopSmooth(percent);
    }

    @Override
    public void setSmoothPercent(float percent, long durationMillis) {
        getSmoothHandler().loopSmooth(percent, durationMillis);
    }

    private SmoothHandler getSmoothHandler() {
        if (smoothHandler == null) {
            smoothHandler = new SmoothHandler(new WeakReference<ISmoothTarget>(this));
        }
        smoothHandler.loopSmooth(percent);
        return smoothHandler;
    }
详情见project 的demo

## LICENSE
```
Copyright (c) 2016 Jacksgong(blog.dreamtobe.cn).

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
